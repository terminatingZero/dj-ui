var lobbyDB;
var myApp = angular.module('DJApp', ['ngRoute', 'ngAnimate', 'ngCookies', 'ngSanitize']);
var userID = 'test';

myApp.config(function ($routeProvider) {
    $routeProvider
        // Home
        .when('/home', {
            templateUrl: './home.html',
            controller: 'HomeCtrl'
        })

        // Dashboard Route
        .when('/lobby/:lobbyID', {
            templateUrl: './lobby.html',
            controller: 'LobbyCtrl',
            hasData: true
        })
        // Default Route
        .otherwise({
            redirectTo: '/home'
        });
});

myApp.controller('HomeCtrl', function ($scope, $rootScope, $location) {
    // Declare the initial value for the scope’s user object that we’ll bind our inputs to.
    // $scope.login = function(){
    //     Spotify.login();
    // };

    // $scope.join = function() {
    //     $location.path('/lobby/' + $scope.lobbyID);
    // };
});

var songs = [
        { "_id":"test", "songID": 12, "title":"title 1", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/1", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 2", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/2", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 3", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/3", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 4", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/4", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 5", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 6", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 1", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 2", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 3", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 4", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 5", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 },
        { "_id":"test", "songID": 12, "title":"title 6", "artist": "Elvis", "album":"album1", "albumArt":"http://lorempixel.com/250/250/nature/5", "voteTotal": 0, "voteType": 0 }
];

myApp.controller('LobbyCtrl', function ($scope, $routeParams, $rootScope, $location) {
    // Declare the initial value for the scope’s user object that we’ll bind our inputs to.
    $scope.songs = songs;
    $scope.isHost = true;
    $scope.lobbyID = $routeParams.lobbyID;

    $scope.init = function(){        
        lobbyDB = new PouchDB("lobbyID");

        lobbyDB.info()
        .catch(function(err) {
            alert("Failed to initialize connection!");
            $location.path("/home");
        });

        // Retrieve all song votes
        // lobbyDB.query({
        //     //set all the thumbs
        //     $.grep(songList, function(song){
        //         alert(song.title);
        //     });
        // });

        var playlistSongs = document.getelementsbyclassname("playlist-song");

        alert(playlistSongs.length);

        $.grep(playlistSongs, function(song){
            alert(song.title);
        });
    };

    $scope.removeSong = function(index) {
        // send update to clients

        //update screen
        $scope.songs.splice(index, 1);
    };

    $scope.upVote = function(index) {
        var song = $scope.songs[index];

        // create/update vote entry
        lobbyDB.get(song._id).then(function(doc){
            doc.voteType = (doc.voteType == 1 ? 0 : 1);
            $scope.songs[index].voteType = doc.voteType;
            lobbyDB.put(doc);
        }).catch(function(err) {
            var pouchObj = {
                _id: song._id,
                voteType: 1
            }; 

            lobbyDB.put(pouchObj);
        });
        // send update to host

         
    };

    $scope.downVote = function(index) {
        var song = $scope.songs[index];
     
        // create/update vote entry
        lobbyDB.get(song._id).then(function(doc){
            doc.voteType = (doc.voteType == -1 ? 0 : -1);
            $scope.songs[index].voteType = doc.voteType;
            lobbyDB.put(doc);
        }).catch(function(err) {
            var pouchObj = {
                _id: song._id,
                voteType: -1
            };
            
            lobbyDB.put(pouchObj);
        });

        // send update to host
    };

    $scope.playNow = function(index) {
        var song = $scope.songs[index];

        // send updates to clients

        // update screen
        $scope.songs.splice(index, 1);
        $scope.songs.splice(0, 1);
        $scope.songs.splice(0, 0, song);
    };

    $scope.playNext = function(index) {
        // send updates to clients

        // update screen
        var nextToPlay = $scope.songs[index];
        $scope.songs.splice(index, 1);
        $scope.songs.splice(1, 0, nextToPlay);
    };
});

myApp.controller('MusicPlayerCtrl', ['$scope', function($scope) {
  $scope.songs = songs;
}])
.directive('musicPlayer', function ($rootScope) {
    return {
        templateUrl: "./directives/music-player.html",
    };
});

// Socket implementation
myApp.factory('socket', function ($rootScope) {
    var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});